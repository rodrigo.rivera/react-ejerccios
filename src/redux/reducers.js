import { createStore } from "redux";


/**
 * esto es un reducer exs para revisar el estado 
 * @param {*} state 
 * @param {*} action 
 */
function counter(state = 0, action) {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
}

/**
 * crea el estado
 */
let storage01 = createStore(counter);



/**
 * te suscribiste a los cambios 
 */
storage01.subscribe(() => {
    console.log("esto es una suscripcion y obtine el estado ", storage01.getState());
});




storage01.dispatch({ type: 'INCREMENT' });


/**
 * 
 * @param {*} state 
 * @param {*} action 
 */
function menu(state = {}, action) {
    switch (action) {
        case 'entidad':
            return state = ["esta es la entidad "];

        case 'municipio':
            return state = ["esto es el municipio"];

        case 'localidad':
            return state = ["esto es la localidad "];

        default:
            break;
    }


}