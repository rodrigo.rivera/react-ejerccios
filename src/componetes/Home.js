import React, { useState } from 'react'
import Counter from "./Counter";

export default function Home(props) {

    const { storeRedux, estadoRedux } = props // estado globales  redux
    /**
     * estado  local 
     * 
     */
    const [numero, setNumero] = useState(0);
    const [menu, setMenu] = useState([]);

    storeRedux.subscribe(() => {
        let temp = storeRedux.getState()
        setNumero(temp);
        console.log("esto es una suscripcion y obtine el estado ", numero);
    });

    /**
     * se cuscribe  para tener cambios
     */
    estadoRedux.subscribe(() => {
        let temp = estadoRedux.getState();
        console.log("cambiar el menu ", temp)
        setMenu(temp)
    })

    /**
     * Funciones que va a cambiar el estado 
     */
    const cambiaEstado = () => {
        estadoRedux.dispatch({ type: 'entidad' })
    }


    const cambioMunicipio = () => {
        estadoRedux.dispatch({ type: 'municipio' })
    }

    const cambiaColonia = () => {
        estadoRedux.dispatch({ type: 'localidad' })
    }
    return (
        <div>

            <Counter value={storeRedux.getState()}
                onIncrement={() => storeRedux.dispatch({ type: 'INCREMENT' })}
                onDecrement={() => storeRedux.dispatch({ type: 'DECREMENT' })}
            />
            <br></br>
            <div>
                valor = [{numero}]
            </div>
            <br></br>
            <div>
                <button onClick={cambiaEstado}>
                    cambia el estado
                </button>
                <button onClick={cambioMunicipio}>
                    cambia el municipio
                </button>
                <button onClick={cambiaColonia} >
                    cambia colonia
                </button>
                <br></br>
                <div>
                    el estado es [{JSON.stringify(menu)}]
                </div>
            </div>
        </div>


    )

}

