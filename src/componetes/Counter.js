
import React from "react";

export default function Counter(props) {





    const { value, onIncrement, onDecrement } = props


    return (
        <p>
            Clicked: {value} times
            {' '}
            <button onClick={onIncrement}>
                +
          </button>
            {' '}
            <button onClick={onDecrement}>
                -
          </button>

        </p>
    )
}
  