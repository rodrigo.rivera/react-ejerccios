import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import './App.css';
// import { createStore } from 'redux';
// import { count, menu } from "./redux/reducer";
// import Home from './componetes/Home';
import Pagina from "./pagina/pagina";
import ErrorPagina from "./pagina/error";

// let store = createStore(count,100)
// let estadoMenu = createStore(menu, JSON.stringify({nombre:"rodrigo"}));
//<Home storeRedux={store} estadoRedux={estadoMenu} />
function App() {
  return (
    <Router>
      <Switch>
        <Route path='/home' component={({match}) => (  <Pagina p1={"props"} p2={"pros2"}  p3={match}/>)} />
        <Route  component={({match}) => (  <ErrorPagina />)} />
        
        
      </Switch>
    </Router>

  );
}

export default App;
